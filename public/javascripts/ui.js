$(document).ready(function(){
    app_init();  
    show_information_form();
    console.log("Loading");
    loadQuestions();

});

function app_init(){
    window.state = {currentQuestionIndex : 0, answers : []};
}

function loadQuestions() {
    $.get("/questions", function (data) {
        window.state.questions = data;
        window.state.questionsLoaded = true;
    });
}

function information_onsubmit(){
    console.log('Information submit');
    var firstName = $('#inputFirstName').val();
    var lastName = $('#inputLastName').val();
    var emailId = $('#inputEmailAddress').val();
    window.state.information = {'firstName': firstName, 'lastName': lastName, 'emailId': emailId};
    show_question_form();
}

function question_onnext(){
    console.log("Next Question")
    save_question_response();
    window.state.currentQuestionIndex++;
    update_questions_form();
    
}

function question_onback(){
    console.log("Previous Question")
    save_question_response();
    window.state.currentQuestionIndex--;
    update_questions_form();
}

function question_onsubmit(){
    save_question_response();
    var response = prepare_response();
    console.log(response);
    save_response(response, function(err, msg){
        show_thanks_form();
    })
}

function show_information_form(){
    $('#information_form').show();
    $('#question_form').hide();
    $('#thanks_form').hide();
}

function show_question_form(){
    $('#information_form').hide();
    $('#question_form').show();
    $('#thanks_form').hide();
    window.state.currentQuestionIndex = 0;
    update_question_n_options();
    update_question_action_buttons();
}


function update_questions_form(){
    if(window.state.currentQuestionIndex < 0 ){
        window.state.currentQuestionIndex = 0;
    }
    if(window.state.currentQuestionIndex >= window.state.questions.length -1){
        window.state.currentQuestionIndex = window.state.questions.length - 1;
    }
    console.log("Question ", window.state.currentQuestionIndex);
    update_question_n_options();
    update_question_action_buttons();
}

function save_question_response(){
    var answer = 0;
    if($('#option1').prop('checked') == true){
        answer = 1;
    }
    else if($('#option2').prop('checked') == true){
        answer = 2;
    }
    else if($('#option3').prop('checked') == true){
        answer = 3;
    }
    else if($('#option4').prop('checked') == true){
        answer = 4;
    }
    window.state.answers[window.state.currentQuestionIndex] = answer;
}

function update_question_n_options(){
    var currentQuestion = window.state.questions[window.state.currentQuestionIndex];
    $('#questionInput').val(currentQuestion.question);
    $('#optionLabel1').text(currentQuestion.option1);
    $('#optionLabel2').text(currentQuestion.option2);
    $('#optionLabel3').text(currentQuestion.option3);
    $('#optionLabel4').text(currentQuestion.option4);

    var questionNo = "Question " + (window.state.currentQuestionIndex + 1) + "/" + (window.state.questions.length);
    $('#questionIndex').text(questionNo);

    var answer = window.state.answers[window.state.currentQuestionIndex];
    console.log('Selected answer ' + answer);
    if(!answer || answer == 0){
        $('#option1').prop('checked', false);    
        $('#option2').prop('checked', false);
        $('#option3').prop('checked', false);
        $('#option4').prop('checked', false);
    } else if(answer == 1){
        $('#option1').prop('checked', true);    
        $('#option2').prop('checked', false);
        $('#option3').prop('checked', false);
        $('#option4').prop('checked', false);
    } else if(answer == 2){
        $('#option1').prop('checked', false);    
        $('#option2').prop('checked', true);
        $('#option3').prop('checked', false);
        $('#option4').prop('checked', false);
    } else if(answer == 3){
        $('#option1').prop('checked', false);    
        $('#option2').prop('checked', false);
        $('#option3').prop('checked', true);
        $('#option4').prop('checked', false);
    } else if(answer == 4){
        $('#option1').prop('checked', false);    
        $('#option2').prop('checked', false);
        $('#option3').prop('checked', false);
        $('#option4').prop('checked', true);
    }

}

function update_question_action_buttons(){
    if(window.state.currentQuestionIndex == 0){
        $('#question_back').hide();
    } else {
        $('#question_back').show();
    }
    
    if(window.state.currentQuestionIndex >= window.state.questions.length -1){
        $('#question_next').hide();
        $('#question_submit').show();
    } else {
        $('#question_next').show();
        $('#question_submit').hide();
    }
}

function prepare_response() {
    var response = {};
    response.information = window.state.information;
    response.answers = [];

    for(questionIndex in window.state.questions){
        var question = window.state.questions[questionIndex];
        var answer = window.state.answers[questionIndex];
        response.answers.push({'questionId' : question.id, 'answer':  answer});
    }

    return response;
}

function save_response(response, callback) {
    $.post("/responses", response, function (data) {
        window.state.responseSave = true;
        callback(false, 'Response saved');
    }, 'json');
}


function show_thanks_form(){
    $('#information_form').hide();
    $('#question_form').hide();
    $('#thanks_form').show();
}