var express = require('express');
var mysql = require('mysql');

var router = express.Router();

let connection = mysql.createConnection({
    host: 'localhost',
    user: 'app',
    password: 'password',
    database: 'questionaire'
});
connection.connect(function(err){
    if(err){
        console.error('Error in connecting to database' + err);
    } else {
        console.log('Questions connected to database');
    }
});

let sql = 'select questionid, questionname, questionoption1, questionoption2, questionoption3, questionoption4 from questions where questionenabled = true;';

/* GET users listing. */
router.get('/', function(req, res, next) {
    connection.query(sql, function(error, result, fields){
        if(error){
            console.error('Error querying the question ' + err);
            res.send([]);
            return ;
        } 
        questions = [];
        for(rowIndex in result){
            var row = result[rowIndex];
            questions.push({
                'id' : row['questionid'],
                'question' : row['questionname'],
                'option1' : row['questionoption1'],
                'option2' : row['questionoption2'],
                'option3' : row['questionoption3'],
                'option4' : row['questionoption4']
            });
        }

        res.send(questions);
    });
});

module.exports = router;
